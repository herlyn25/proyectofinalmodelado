from django.contrib import admin

# Register your models here.
from limpieza.models import *


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ['name', 'url', 'duration']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'dues', 'url', 'sending']
