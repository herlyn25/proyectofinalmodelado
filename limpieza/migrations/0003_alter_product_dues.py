# Generated by Django 4.0.6 on 2022-08-06 04:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('limpieza', '0002_characterdbz_product'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='dues',
            field=models.CharField(max_length=255),
        ),
    ]
