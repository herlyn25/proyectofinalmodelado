from rest_framework.viewsets import ModelViewSet

from limpieza.models import *
from limpieza.serializer import *


class MovieViewSet(ModelViewSet):
    queryset=Movie.objects.all()
    serializer_class = MovieSerializer


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer