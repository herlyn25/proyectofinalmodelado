from django.db import models


# Create your models here.

class Movie(models.Model):
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    duration = models.CharField(max_length=15)


class CharacterDBZ(models.Model):
    id_character = models.IntegerField()
    name = models.CharField(max_length=255)
    universe = models.CharField(max_length=200)
    image = models.CharField(max_length=255)
    specie = models.CharField(max_length=255)

class Product(models.Model):
    name = models.CharField(max_length=255)
    price = models.IntegerField()
    dues = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    sending = models.CharField(max_length=255)