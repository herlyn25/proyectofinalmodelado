from rest_framework import routers

from limpieza.api import *

router = routers.DefaultRouter()
router.register('movie', MovieViewSet)
router.register('product', ProductViewSet)
